package com.project.challange.repository;

import com.project.challange.model.Registers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

//Untuk membantu mengmaping repository
@Repository
public interface RegisterRepository extends JpaRepository<Registers, Integer> {
    //membuat query agar email tidak sama
    Optional<Registers> findByEmail(String email);

  Optional<Registers> findByPassword(String password);

}
