package com.project.challange.model;

import javax.persistence.*;

//mengmaping table di database
@Entity
//Untuk membuat table di database
@Table(name = "challange")
public class Registers {
    //membuat column id agar value tidak sama
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
//membuat colum username
    @Column(name = "username")
    private String username;
    //membuat colum email
    @Column(name = "email")
    private String email;
    //membuat colum password
    @Column(name = "password")
    private String password;
//dibutuhkan untuk edit agar tidak perlu memanggil nama colum satu persatu
    public Registers() {
    }
//membuat getter dan setter untuk memudahkan dalam membuat method edit
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
