package com.project.challange.service_impl;

import com.project.challange.model.Registers;
import com.project.challange.repository.RegisterRepository;
import com.project.challange.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RegisterImpls implements RegisterService {
    //memanggil repository
    @Autowired
    private RegisterRepository registerRepository;

    //membuat method add user
    @Override
    public Registers addUser(Registers registers) {
        var validasi = registerRepository.findByEmail(registers.getEmail());
        if (validasi.isPresent()) {
           return null;
        }
        return registerRepository.save(registers);
    }

    //membuat method get user
    @Override
    public Registers getUser(Integer id) {
        return registerRepository.findById(id).get();
    }

    //membuat method get all
    @Override
    public List<Registers> getAll() {
        return registerRepository.findAll();
    }

    //membuat method edit user
    @Override
    public Registers editUser(Integer id, Registers register) {
        Registers update = registerRepository.findById(id).get();
        update.setEmail(register.getEmail());
        update.setPassword(register.getPassword());
        update.setUsername(register.getUsername());
        var validasi = registerRepository.findByEmail(register.getEmail());
        if (validasi.isPresent()) {
            return null;
        }
        return registerRepository.save(update);
    }

    //membuat method delete user
    @Override
    public Map<String, Boolean> deleteUsers(Integer id) {
        try {
            registerRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("Hapus", Boolean.TRUE);
            return res;
        } catch (Exception e) {
           return null;
        }
    }

    @Override
    public  Map<String, Object> login(Registers registers) {
       Registers user = registerRepository.findByPassword(registers.getPassword()).get();
        Registers users = registerRepository.findByEmail(registers.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("user", users);
        response.put("user" , user);
        return response;
    }
}
