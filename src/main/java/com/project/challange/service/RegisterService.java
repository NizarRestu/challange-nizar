package com.project.challange.service;

import com.project.challange.model.Registers;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public interface RegisterService {
    //Method yang akan dipanggil di service impl
    //membuat method tambah user / register
    Registers addUser(Registers registers);

    //membuat method get user / menapilkan satu user
    Registers getUser(Integer id);

    //membuat method get all / menampilkan semua user
    List<Registers> getAll();

    //membuat method edit user
    Registers editUser(Integer id, Registers registers);

    //delete menggunakan map agar ada response dari bodynya
    //membuat method delete user
    Map<String, Boolean> deleteUsers(Integer id);

    //membuat login
    Map<String, Object>  login(Registers registers);

}
