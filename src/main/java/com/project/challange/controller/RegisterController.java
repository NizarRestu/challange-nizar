package com.project.challange.controller;

import com.project.challange.model.Registers;
import com.project.challange.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
//membuat path utamanya
@RequestMapping("/register")
@CrossOrigin(origins = "http://localhost:3000")
public class RegisterController {
    //memanggil servicenya
    @Autowired
    private RegisterService registerService;

    //membuat requestnya / response add user
    @PostMapping(path = "/reg")
    public Registers register(@RequestBody Registers registers) {
        return registerService.addUser(registers);
    }
    //membuat requestnya / response get  user
    @GetMapping(path = "/{id}")
    public Registers getUser(@PathVariable("id") Integer id) {
        return registerService.getUser(id);
    }
    //membuat requestnya / response edit user
    @PutMapping(path = "/{id}")
    public Registers editUser(@PathVariable("id") Integer id, @RequestBody Registers registers) {
        return registerService.editUser(id ,registers);
    }
    //membuat requestnya / response all user
    @GetMapping
    public List<Registers> getAll(){
        return registerService.getAll();
    }
    //membuat requestnya / response delete user
    @DeleteMapping("/{id}")
    public Map<String, Boolean> delete(@PathVariable("id")  Integer id) {
        return registerService.deleteUsers(id);
    }
    //membuat requestnya / response login user
    @PostMapping("/sign-in")
    public Map<String ,Object> loginUser(@RequestBody Registers registers) {
        return registerService.login(registers);
    }
}
